#!/bin/bash

HERE=`pwd -P`

if [ -z "$1" ]; then
    echo "The version: ex) 16 (NKRV)"
    exit 1
fi

echorunvv()
{
    echo "`date`"
    echo "$1"
    $1
}

# nkjv 20 esv 21 lut 22 vul 23 lsg 24 riv 25 nvi 26

# hnv 27 ncv 28 nirv 29 ojb 30
# bbe 31 cjb 32 tmb 33 web 34 ylt 35
# dby 36 wbt 37 tyn 38 wyc 39 sblg 40
# wnt 41 bla 42 jbs 43 rvr 44 ntv 45
# sev 46 aa 47 nvip 48 ost 49 elb 50
# gdb 51 svv 52 cuvp 53 asv 54 ceb 55
# gw 56 gnt 57 csb 58 jub 59 leb 60
# rhe 61 nlt 62 nrs 63 rsv 64 msg 65

# kjv 66 nas 67 niv 68 cuvs 69 cuv 70 - second versions?

aver=(KRV NKRV 공동번역 표준새번역 쉬운성경
      NIV KJV NASB Japanese_SKY Japanese_KGY
      Japanese_SKDY Hebrew_Pr Greek_Pr Chinese_Trad Chinese_Simp
      Urimal 흠정역 Greek Hebrew nkjv
      esv lut vul lsg riv
      nvi hnv ncv nirv ojb
      bbe cjb tmb web ylt
      dby wbt tyn wyc sblg
      wnt bla jbs rvr ntv
      sev aa nvip ost elb
      gdb svv cuvp asv ceb
      gw gnt csb jub leb
      rhe nlt nrs rsv msg      
      kjv nas niv cuvs cuv)
bver=$1
jver=$(printf "%02d" ${bver})
mver=$(printf "%d" $(( $bver - 1 )))

vn=9
declare -A vols
for ((ii=0; ii<${vn}; ii++)); do
    jj=$(printf "%02d" $(( $ii + 1 )))
    vols[$ii]=${HERE}/volraw_${jj}_${jver}.dat
    # echo "${vols[$ii]}"
done

turl="https://www.biblestudytools.com/${aver[$mver]}/"
echorunvv "wget ${turl}"
echorunvv "rm wget-log"
echorunvv "rm wget-log.*"
echorunvv "mv index.html ${vols[0]}"
sed '/btn btn-lg btn-block bst-button-white/!d' ${vols[0]} > ${vols[1]}
sed "s/                                            //g" ${vols[1]} > ${vols[2]}
sed 's/^.*href/href/' ${vols[2]} > ${vols[3]}
sed "s/https:\/\/www.biblestudytools.com\/${aver[$mver]}\///g" ${vols[3]} > ${vols[4]}
sed 's/href="//g' ${vols[4]} > ${vols[5]}
sed 's/\/">//g' ${vols[5]} > ${vols[6]}
tr "\n\r" "\t" < ${vols[6]} > ${vols[7]}
echo `cat ${vols[7]}` > ${vols[8]}
avol=(`cat ${vols[8]}`)

for ((ii=0; ii<${vn}; ii++)); do
    echorunvv "rm ${vols[$ii]}"
done

nvol=66
chapar=(50 40 27 36 34 24 21 4 31 24 22 25 29 36 10 13 10 42 150 31 12 8 66 52 5 48 12 14 3 9 1 4 7 3 3 3 2 14 4 28 16 24 21 28 16 16 13 6 6 4 4 5 3 6 4 3 1 13 5 5 3 5 1 1 1 22)

jver=$(printf "%02d" ${bver})

for ((ivol=0; ivol<${nvol}; ivol++)); do
    bvol=$(( $ivol + 1 ))
    jvol=$(printf "%02d" ${bvol})
    for ((icha=0; icha<${chapar[ivol]}; icha++)); do
	bcha=$(( $icha + 1 ))
	jcha=$(printf "%03d" ${bcha})
	echorunvv "source 02_download_raw_01.sh ${bver} ${bvol} ${bcha}"
	if [ ! -d "raw_${nn}" ]; then
	    echorunvv "mkdir raw_${nn}"
	fi
	if [ ! -d "raw_${nn}/${jver}" ]; then
	    echorunvv "mkdir raw_${nn}/${jver}"
	fi
	echorunvv "mv raw_${nn}_${jver}_${jvol}_${jcha}.dat raw_${nn}/${jver}/."
    done
done

if [ ! -d "data" ]; then
    echorunvv "mkdir data"
fi

echorunvv "mv raw_${nn}/${jver} data/."
echorunvv "cd ${HERE}/data/${jver}"
# echorunvv "rename -s raw_15_ bible_ *.dat"
echorunvv "rename raw_${nn}_ bible_ *.dat"
echorunvv "cd ../.."
