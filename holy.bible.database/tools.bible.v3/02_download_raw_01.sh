#!/bin/bash

HERE=`pwd -P`

echorunvv()
{
    echo "`date`"
    echo "$1"
    $1
}

bver=$1
bvol=$2
bcha=$3

jvol=$(printf "%02d" ${bvol})
jcha=$(printf "%03d" ${bcha})

mvol=$(printf "%d" $(( $bvol - 1 )))
mcha=$(printf "%d" $(( $bcha - 1 )))

# url="http://www.c3tv.com/m/bible/bible_view.asp?version=${bver}&oldnew=1&volume=${bvol}&jang=${bcha}&app="
# url="http://goodtvbible.com/bible.asp?bible_idx=${bvol}&bible_version_1=${bver}&jang_idx=${bcha}&otnt=${btst}"
url="https://www.biblestudytools.com/${aver[$mver]}/${avol[$mvol]}/${bcha}.html"

# echorunvv "python get.py '${url}' ${bver} ${bvol} ${bcha}"

nn=22
# nn=17

declare -A rawinput
for ((ii=0; ii<${nn}; ii++)); do
    jj=$(printf "%02d" $(( $ii + 1 )))
    rawinput[$ii]=${HERE}/raw_${jj}_${jver}_${jvol}_${jcha}.dat
    # echo "${rawinput[$ii]}"
done

echorunvv "wget ${url}"
echorunvv "rm wget-log"
echorunvv "rm wget-log.*"
echorunvv "mv ${bcha}.html ${rawinput[0]}"

# output=bible_${bver}_${bvol}_${bcha}.dat

grep -A 2 -h 'span class="verse-[0-9]*' ${rawinput[0]} > ${rawinput[1]}
sed "s/                            //g" ${rawinput[1]} > ${rawinput[2]}
tr -d '\015' < ${rawinput[2]} > ${rawinput[3]}
sed 's/<span class="verse-number">//' ${rawinput[3]} > ${rawinput[4]}
sed 's/<span class="verse-.[0-9]*">//' ${rawinput[4]} > ${rawinput[5]}
sed 's/<\/span>//' ${rawinput[5]} > ${rawinput[6]}
sed 's/--//g' ${rawinput[6]} > ${rawinput[7]}
tr -d '\n' < ${rawinput[7]} > ${rawinput[8]}
sed 's/    //g' ${rawinput[8]} > ${rawinput[9]}
sed 's/<strong>/\
/g' ${rawinput[9]} > ${rawinput[10]}
sed "s/<\/strong>/ ==$(printf "\t")/g" ${rawinput[10]} > ${rawinput[11]}
sed 's/<sup class="verse-reference verse-footnote" data-verseid=".[0-9]*" data-identifier="fn.[0-9]*"><a href="javascript:void(0);">.[a-z]*<\/a><\/sup> //' ${rawinput[11]} > ${rawinput[12]}
# sed 's/<\/sup>/<\/sup> /g' ${rawinput[12]} > ${rawinput[13]}
sed 's/>/> /g' ${rawinput[12]} > ${rawinput[13]}
sed 's/<sup class="verse-reference verse-crossreference">//g' ${rawinput[13]} > ${rawinput[14]}
sed 's/<a href="javascript:void(0);">//g' ${rawinput[14]} > ${rawinput[15]}
sed 's/.[0-9]*<\/a>//g' ${rawinput[15]} > ${rawinput[16]}
sed 's/<\/sup>//g' ${rawinput[16]} > ${rawinput[17]}
sed 's/  / /g' ${rawinput[17]} > ${rawinput[18]}
sed 's/  / /g' ${rawinput[18]} > ${rawinput[19]}
sed 's/<sup class="verse-reference verse-footnote" data-verseid=".[0-9]*" data-identifier="fn.[0-9]*"> //' ${rawinput[19]} > ${rawinput[20]}
sed '/^$/d' ${rawinput[20]} > ${rawinput[21]}

nnp=$(( $nn - 1 ))

for ((ii=0; ii<${nnp}; ii++)); do
    echorunvv "rm ${rawinput[$ii]}"
done

# sed '/&nbsp;&nbsp;<\/b>/!d' ${rawinput[0]} > ${rawinput[1]}
# echorunvv "rm ${rawinput[0]}"


# echorunvv "rm ${raw01put} ${raw02put} ${raw03put} ${raw04put} ${raw05put} ${raw06put} ${raw07put} ${raw08put} ${raw09put} ${raw10put} ${raw11put} ${raw12put} ${raw13put} ${raw14put} ${raw15put}"
