#!/usr/local/bin/bash

HERE=`pwd -P`

if [ -z "$1" ]; then
    echo "The version: ex) 16 (NKRV)"
    exit 1
fi

echorunvv()
{
    echo "`date`"
    echo "$1"
    $1
}

bver=$1
nvol=66
# bvol=$2
# nvol=66

chapar=(50 40 27 36 34 24 21 4 31 24 22 25 29 36 10 13 10 42 150 31 12 8 66 52 5 48 12 14 3 9 1 4 7 3 3 3 2 14 4 28 16 24 21 28 16 16 13 6 6 4 4 5 3 6 4 3 1 13 5 5 3 5 1 1 1 22)

if [ ! -d "raw_02" ]; then
    echorunvv "mkdir raw_02"
fi

jver=$(printf "%02d" ${bver})
if [ ! -d "raw_02/${jver}" ]; then
    echorunvv "mkdir raw_02/${jver}"
fi

for ((ivol=0; ivol<${nvol}; ivol++)); do
    bvol=$(( $ivol + 1 ))
    jvol=$(printf "%02d" ${bvol})
    for ((icha=0; icha<${chapar[ivol]}; icha++)); do
	bcha=$(( $icha + 1 ))
	jcha=$(printf "%03d" ${bcha})
	echorunvv "source 01_dl_raw.sh ${bver} ${bvol} ${bcha}"
	echorunvv "mv raw_02_${jver}_${jvol}_${jcha}.dat raw_02/${jver}/."
    done
done
