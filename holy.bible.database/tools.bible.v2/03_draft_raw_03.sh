#!/bin/bash

HERE=`pwd -P`

echorunvv()
{
    echo "`date`"
    echo "$1"
    $1
}

bver=$1
jver=$(printf "%02d" ${bver})

nvol=66

chapar=(50 40 27 36 34 24 21 4 31 24 22 25 29 36 10 13 10 42 150 31 12 8 66 52 5 48 12 14 3 9 1 4 7 3 3 3 2 14 4 28 16 24 21 28 16 16 13 6 6 4 4 5 3 6 4 3 1 13 5 5 3 5 1 1 1 22)

declare -A rawdir
declare -A rawsubdir
declare -A rawinput

nraw=20		# number of intermediate raw data

for ((ivol=0; ivol<${nvol}; ivol++)); do
    bvol=$(( $ivol + 1 ))
    jvol=$(printf "%02d" ${bvol})
    for ((icha=0; icha<${chapar[ivol]}; icha++)); do
	bcha=$(( $icha + 1 ))
	jcha=$(printf "%03d" ${bcha})

	for ((iraw=0; iraw<${nraw}; iraw++)); do
	    jraw=$(printf "%02d" $(( $iraw + 1 )))

	    rawdir[$iraw]=${HERE}/raw_${jraw}
	    if [ ! -d "${HERE}/raw_${jraw}" ]; then
		echorunvv "mkdir raw_${jraw}"
	    fi

	    rawsubdir[$iraw]=${HERE}/raw_${jraw}/${jver}    
	    if [ ! -d "${HERE}/raw_${jraw}/${jver}" ]; then
		echorunvv "mkdir raw_${jraw}/${jver}"
	    fi
	    
	    rawinput[$iraw]=${HERE}/raw_${jraw}/${jver}/raw_${jraw}_${jver}_${jvol}_${jcha}.dat
	    # echo "${rawinput[$iraw]}"
	done			# iraw
	if [ ! -f "${rawinput[1]}" ]; then
	    continue
	fi	
	sed 'N;s/\n/---/g' ${rawinput[1]} > ${rawinput[2]}
	echo "${rawinput[2]}"
	sed 's/---.*$//g' ${rawinput[2]} > ${rawinput[3]}
	echo "${rawinput[3]}"
	sed 'N;s/\n/---/g' ${rawinput[3]} > ${rawinput[4]}
	echo "${rawinput[4]}"
	sed 's/---.*$//g' ${rawinput[4]} > ${rawinput[5]}
	echo "${rawinput[5]}"
	sed -e 's/^ *//g' -e 's/ *$//g'	${rawinput[5]} > ${rawinput[6]}
	echo "${rawinput[6]}"
	tr -d "\n" < ${rawinput[6]} > ${rawinput[7]}
	echo "${rawinput[7]}"
	sed 's/<br>//g' ${rawinput[7]} > ${rawinput[8]}
	echo "${rawinput[8]}"
	sed 's/<b>/\
<b>/g' ${rawinput[8]} > ${rawinput[9]}
	echo "${rawinput[9]}"
	sed "s/<a style='cursor: pointer' id='hyper_.[0-9]*' onclick='showBiDic(true, .[0-9]*, this)'>//g" ${rawinput[9]} > ${rawinput[10]}
	echo "${rawinput[10]}"
	sed 's/<span class.*span>/<span>/' ${rawinput[10]} > ${rawinput[11]}
	echo "${rawinput[11]}"
	sed 's/<span>//g' ${rawinput[11]} > ${rawinput[12]}
	echo "${rawinput[12]}"
	sed 's/<\/a>//g' ${rawinput[12]} > ${rawinput[13]}
	echo "${rawinput[13]}"
	sed 's/.&nbsp;&nbsp;//g' ${rawinput[13]} > ${rawinput[14]}
	echo "${rawinput[14]}"
	sed 's/<b>//g' ${rawinput[14]} > ${rawinput[15]}
	echo "${rawinput[15]}"
	sed 's/<\/b>/==/g' ${rawinput[15]} > ${rawinput[16]}
	echo "${rawinput[16]}"
	sed "s/==/ ==$(printf "\t")/g" ${rawinput[16]} > ${rawinput[17]}
	echo "${rawinput[17]}"
	tr -d '\015' < ${rawinput[17]} > ${rawinput[18]}
	echo "${rawinput[18]}"
	sed '/^$/d' ${rawinput[18]} > ${rawinput[19]}
	echo "${rawinput[19]}"
    done			# icha
done				# ivol
