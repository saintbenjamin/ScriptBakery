#! /usr/bin/env python
#---------------------
import sys, os
import urllib2

bibleurl = sys.argv[1]
biblever = sys.argv[2]
biblevol = sys.argv[3]
biblecha = sys.argv[4]

# bver = print("%02d", biblever)
# bvol = print("%02d", biblevol)
# bcha = print("%02d", biblecha)

f = urllib2.urlopen(bibleurl)
# res = f.read()
# print(res)

datname = "raw_01"+"_"+biblever+"_"+biblevol+"_"+biblecha+".dat"
datfile = open(datname,'w')
res = f.read()
datfile.write(res)
datfile.close()
