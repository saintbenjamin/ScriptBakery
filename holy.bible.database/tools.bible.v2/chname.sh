#!/bin/bash

HERE=`pwd -P`

echorunvv()
{
    echo "`date`"
    echo "$1"
    $1
}

nver=19
nvol=66

for ((ii=0; ii<${nver}; ii++)); do
    version=$(printf "%02d" $(( $ii + 1 )))
    echorunvv "cd ${HERE}/data/${version}"
    # echorunvv "rename raw_16_ bible_ *.dat"
    echorunvv "rename -s raw_17_ bible_ *.dat"
    echorunvv "cd .."
done
