#!/usr/local/bin/bash

HERE=`pwd -P`

echorunvv()
{
    echo "`date`"
    echo "$1"
    $1
}

bver=$1
bvol=$2
bcha=$3

if (( $(echo "$(printf %d ${bvol}) < 40" | bc -l) )); then
    btst=1
elif (( $(echo "$(printf %d ${bvol}) >= 40" | bc -l) )); then
    btst=2
fi

# url="http://www.c3tv.com/m/bible/bible_view.asp?version=${bver}&oldnew=1&volume=${bvol}&jang=${bcha}&app="
url="http://goodtvbible.com/bible.asp?bible_idx=${bvol}&bible_version_1=${bver}&jang_idx=${bcha}&otnt=${btst}"

echorunvv "python get.py "${url}" ${bver} ${bvol} ${bcha}"
# echorunvv "python ../../../get.py "${url}" ${bver} ${bvol} ${bcha}"

declare -A rawinput
nn=2
for ((ii=0; ii<${nn}; ii++)); do
    jj=$(printf "%02d" $(( $ii + 1 )))
    jver=$(printf "%02d" ${bver})
    jvol=$(printf "%02d" ${bvol})
    jcha=$(printf "%03d" ${bcha})
    rawinput[$ii]=${HERE}/raw_${jj}_${jver}_${jvol}_${jcha}.dat
    # echo "${rawinput[$ii]}"
done
rawinput[0]=${HERE}/raw_01_${bver}_${bvol}_${bcha}.dat

# output=bible_${bver}_${bvol}_${bcha}.dat
sed '/&nbsp;&nbsp;<\/b>/!d' ${rawinput[0]} > ${rawinput[1]}
echorunvv "rm ${rawinput[0]}"


# echorunvv "rm ${raw01put} ${raw02put} ${raw03put} ${raw04put} ${raw05put} ${raw06put} ${raw07put} ${raw08put} ${raw09put} ${raw10put} ${raw11put} ${raw12put} ${raw13put} ${raw14put} ${raw15put}"
