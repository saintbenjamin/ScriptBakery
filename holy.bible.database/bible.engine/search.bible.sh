#!/bin/bash

HERE=~/bin/bible.engine
# echo $HERE

echorunvv()
{
    echo "`date`"
    echo "$1"
    $1
}

echohelp()
{
    echo "Usage 1: search.bible [version] [string]"
    echo "ex) search.bible NIV For God so loved the world"
    echo ""
}

errver()
{
    echo "Input proper version among below."
    cat $1 | tr "\n\r" "\t" > $3
    echo `cat $3`
    rm $1 $2 $3
}

wordcount=$#
allwords=$@
words=($allwords)

version=$1
vertab=${HERE}/data/index.version
voltab=${HERE}/data/index.volume

vertmp1=${HERE}/data/index.version.tmp1
awk -F";" '{print $1}' ${vertab} > ${vertmp1}
voltmp1=${HERE}/data/index.volume.tmp1
awk -F";" '{print $1}' ${voltab} > ${voltmp1}

if [[ -z $version ]]; then
    echohelp
    verone=${HERE}/data/index.version.oneline
    errver ${vertmp1} ${voltmp1} ${verone}
    exit 1
fi

if [[ -z `grep -i -x "${version}" ${vertmp1}` ]]; then
    echohelp
    verone=${HERE}/data/index.version.oneline
    errver ${vertmp1} ${voltmp1} ${verone}
    exit 1

elif [ "$version" = "--help" ]; then
    echohelp
    rm ${vertmp1} ${voltmp1}
    exit 0
else
    verstep=`awk -F";" -v tver="$version" '$1==tver { if (NR==0 || $1=="") exit 1; else print $2}' ${vertab}`
    verstjj=$(printf "%02d" ${verstep})
fi

# voltot=`ls -d ${HERE}/data/${verstjj}/bible_${verstjj}_* | wc -l`
voltot=66
for ((jj=0; jj<${voltot}; jj++)); do
    volstep=$(( $jj + 1 ))
    volstjj=$(printf "%02d" ${volstep})
    chaptot=`ls -d ${HERE}/data/${verstjj}/bible_${verstjj}_${volstjj}_* | wc -l 2> /dev/null`
    for ((kk=0; kk<${chaptot}; kk++)); do
	chapstep=$(( kk + 1 ))
	chapstjj=$(printf "%03d" ${chapstep})
	
	cmdfile1=${HERE}/data/command.intermediate
	cmdfile2=${HERE}/data/command.intermediate2
	cmdfile3=${HERE}/data/command.search
		
	# AND
	echo -e "cat ${HERE}/data/${verstjj}/bible_${verstjj}_${volstjj}_${chapstjj}.dat " > ${cmdfile1}
	for (( i=1; i<$wordcount; i++ )); do
	    echo -e "| grep -i ${words[i]}" >> ${cmdfile1}
	done
	cat ${cmdfile1} | tr "\n\r" "   " > ${cmdfile2}
	sed 's/   //g' ${cmdfile2} > ${cmdfile3}
		
	result1=result.${volstep}.${chapstep}
	
	source ${cmdfile3} > ${result1}
	rm ${cmdfile1} ${cmdfile2} ${cmdfile3}
	
	FILESIZE=$(wc -c "${result1}" | awk '{print $1}')
	if [ $FILESIZE -lt 1 ];then
	    rm ${result1}
	fi

	if [ -e ${result1} ]; then
	    rvltmp1=${HERE}/data/res.rvltmp1
	    rvltmp2=${HERE}/data/res.rvltmp2
	    awk -F";" -v tvol="$volstep" '$2==tvol {print $1}' ${voltab} > ${rvltmp1}
	    cat ${rvltmp1} | tr "\n\r" "\t" > ${rvltmp2}
	    volume=(`cat $rvltmp2`)
	    # echo ${volume[@]}
	    rm ${rvltmp1} ${rvltmp2}
	    linum=`wc -l ${result1} | awk '{print $1}'`
	    wdnum=(`awk -F"==\t" '{print $1}' ${result1}`)
	    for ((istep=0; istep<${linum}; istep++)); do
		outputtmp=${HERE}/data/words.tmp
    		output=`awk -F"==\t" -v bwrd="${wdnum[istep]}" '$1==bwrd {print $2}' ${result1}`
		echo "$output"
		echo "$output" > ${outputtmp}
		echo "${volume[0]} ${chapstep}:${wdnum[istep]} (${version})"
		declare -A wdctmp
		declare -A wdctmp2
		declare -A wdctmp3
		declare -A wdctmp4
		for (( i=1; i<$wordcount; i++ )); do
		    j=$(( $i - 1 ))
		    wdctmp3[$j]=${HERE}/data/word.count.$i.tmp
		    wdctmp4[$j]=${HERE}/data/word.count2.$i.tmp
		done
		for (( i=1; i<$wordcount; i++ )); do
		    j=$(( $i - 1 ))
		    wdctmp[$j]=`grep -o -i ${words[i]} ${outputtmp} | wc -l`
		    echo ${wdctmp[$j]} >> ${wdctmp3[$j]}
		    awk '{print(total += $0)}' ${wdctmp3[$j]} > ${wdctmp4[$j]}
		done
	    done # istep
	    rm ${result1} ${outputtmp}
	fi
    done # kk
done # jj

for (( i=1; i<$wordcount; i++ )); do
    j=$(( $i - 1 ))
    wdctmp2[$j]=`tail -n 1 ${wdctmp4[$j]}`
    echo "${words[i]}: ${wdctmp2[$j]}"
done

rm ${vertmp1} ${voltmp1}
for (( i=1; i<$wordcount; i++ )); do
    j=$(( $i - 1 ))
    rm ${wdctmp3[$j]} ${wdctmp4[$j]}
done
