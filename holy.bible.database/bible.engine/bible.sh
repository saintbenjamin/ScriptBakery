#!/bin/bash

HERE=~/bin/bible.engine

echorunvv()
{
    echo "`date`"
    echo "$1"
    $1
}

echohelp()
{
    echo "Usage: bible [version(s)] [volume] [chapter]:[word1](-[word2])"
    echo "ex1) bible NKRV NIV KJV John 3"
    echo "ex2) bible NKRV NIV John 3:16"
    echo "ex3) bible NKRV John 3:16-21"
    echo ""
}

argvcount=$#
allargvs=$@
argv=($allargvs)

vertab=${HERE}/data/index.version
voltab=${HERE}/data/index.volume

vertmp1=${HERE}/data/index.version.tmp1
awk -F";" '{print $1}' ${vertab} > ${vertmp1}
voltmp1=${HERE}/data/index.volume.tmp1
awk -F";" '{print $1}' ${voltab} > ${voltmp1}

# version=$1

if [[ -z $1 ]]; then
    echohelp
    echo "Input proper version among below."
    verone=${HERE}/data/index.version.oneline
    cat ${vertmp1} | tr "\n\r" "\t" > ${verone}
    echo `cat ${verone}`
    rm ${vertmp1} ${voltmp1} ${verone}
    exit 1
fi

# for finargv; do true; done
# echo $finargv			# show final argument

verargvs=$(( $argvcount - 2 )) 	# last two arguments: ex) John 3:14-16

declare -A bver

for ((ii=0; ii<$verargvs; ii++)); do
    if [[ -z ${argv[$ii]} ]]; then
	echohelp
	echo "Input proper version among below."
	verone=${HERE}/data/index.version.oneline
	cat ${vertmp1} | tr "\n\r" "\t" > ${verone}
	echo `cat ${verone}`
	rm ${vertmp1} ${voltmp1} ${verone}
	exit 101
	
    elif [[ -z `grep -i -x "${argv[$ii]}" ${vertmp1}` ]]; then
	echohelp
	echo "Input proper version among below."
	verone=${HERE}/data/index.version.oneline
	cat ${vertmp1} | tr "\n\r" "\t" > ${verone}
	echo `cat ${verone}`
	rm ${vertmp1} ${voltmp1} ${verone}
	exit 102
	
    elif [ "${argv[$ii]}" = "--help" ]; then
	echohelp
	rm ${vertmp1} ${voltmp1}
	exit 103
    else
	bver[$ii]=`awk -F";" -v tver="${argv[$ii]}" '$1==tver { if (NR==0 || $1=="") exit 1; else print $2}' ${vertab}`
	jver[$ii]=$(printf "%02d" ${bver[$ii]})
	# echo "version $ii: ${bver[$ii]}"
    fi

done # ii

volargv=$(( $argvcount - 2 ))
volume=${argv[$volargv]}
# echo "volume: $volume"

wrdargv=$(( $argvcount - 1 ))
word=${argv[$wrdargv]}
# echo "word: $word"

if [[ -n `grep -i -x "${volume}" ${vertmp1}` ]]; then
    if [[ -n `grep -i -x "${word}" ${voltmp1}` ]]; then
	bver[$ii]=`awk -F";" -v tver="${argv[$ii]}" '$1==tver { if (NR==0 || $1=="") exit 1; else print $2}' ${vertab}`
	jver[$ii]=$(printf "%02d" ${bver[$ii]})
	bvolt=`awk -F";" -v tvol="$word" '$1==tvol {print $2}' ${voltab}`
	# echo "Word idx: $bvolt"
	jvolt=$(printf "%02d" ${bvolt})
	numchap=`ls ${HERE}/data/${jver[0]}/bible_${jver[0]}_${jvolt}_* | wc -l`
	echohelp
	echo "$word has totally ${numchap} chapters. Input proper chapter."
	rm ${vertmp1} ${voltmp1} ${voltmp2} ${volone}
	exit 104
    else
	echohelp
	echo "Input proper volume among below."
	voltmp2=${HERE}/data/index.volume.tmp2
	volone=${HERE}/data/index.volume.oneline
	cat ${voltmp1} | tr "\n\r" "\t" > ${voltmp2}
	cat ${voltmp2} | sed 's/---/;/g' | tr ";" "\n" > ${volone}
	echo `cat ${volone}`
	rm ${vertmp1} ${voltmp1} ${voltmp2} ${volone}
	exit 105
    fi
elif [[ -z `grep -i -x "${volume}" ${voltmp1}` ]]; then
    echohelp
    echo "Input proper volume among below."
    voltmp2=${HERE}/data/index.volume.tmp2
    volone=${HERE}/data/index.volume.oneline
    cat ${voltmp1} | tr "\n\r" "\t" > ${voltmp2}
    cat ${voltmp2} | sed 's/---/;/g' | tr ";" "\n" > ${volone}
    echo `cat ${volone}`
    rm ${vertmp1} ${voltmp1} ${voltmp2} ${volone}
    exit 106
else 
    bvol=`awk -F";" -v tvol="$volume" '$1==tvol {print $2}' ${voltab}`
    jvol=$(printf "%02d" ${bvol})
fi

tableout=$volume

declare -A output
declare -A input

bcha=`echo $word | awk -F":" '{print $1}'`
jcha=$(printf "%03d" ${bcha})

if [[ -z `echo $word | awk -F":" '{print $2}'` ]]; then
    for ((ii=0; ii<$verargvs; ii++)); do
	input[$ii]=${HERE}/data/${jver[$ii]}/bible_${jver[$ii]}_${jvol}_${jcha}.dat
	if [[ ! -e ${input} ]]; then
	    bver[$ii]=`awk -F";" -v tver="${argv[$ii]}" '$1==tver { if (NR==0 || $1=="") exit 1; else print $2}' ${vertab}`
	    jver[$ii]=$(printf "%02d" ${bver[$ii]})
	    numchap=`ls ${HERE}/data/${jver[0]}/bible_${jver[0]}_${jvolt}_* | wc -l`
	    echohelp
	    echo "$volume has totally ${numchap} chapters. Input proper chapter."
	    rm ${vertmp1} ${voltmp1}
	    exit 107
	fi
    done
    linum=`wc -l ${input} | awk '{print $1}'`
    # linum=$((`wc -l ${input} | awk '{print $1}'` - 1)) # 200326 Benji debug. Useless one more line is removed.
    for ((istep=0; istep<${linum}; istep++)); do
    	iwrd=$(( $istep + 1 ))
	for ((ii=0; ii<$verargvs; ii++)); do
	    output[$ii]=`awk -F"==\t" -v bwrd="$iwrd" '$1==bwrd {$1=""; print $0}' ${input[$ii]}`
	done
	for ((ii=0; ii<$verargvs; ii++)); do
	    if [ "${jver[ii]}" = "19" ]; then
		# echo -n "$word:$iwrd   " && echo -e "${output[$ii]}" | rev
		echo -n "$word:$iwrd   " && echo "${output[$ii]}" | rev
	    else
		echo "$word:$iwrd   ${output[$ii]}"
	    fi
	done
	if [[ $verargvs -gt 1 ]]; then
	    echo ""
	fi
    done
    if [[ $verargvs -gt 1 ]]; then
	veridxtmp=${HERE}/data/veridx.tmp
	veridxtmp2=${HERE}/data/veridx.tmp2
	for ((ii=0; ii<$verargvs; ii++)); do
	    veridx=$ii
	    echo "${argv[$veridx]}" >> ${veridxtmp}
	done
	cat ${veridxtmp} | tr "\n" "," | sed 's/.$//' > ${veridxtmp2}
	mulver=`cat ${veridxtmp2}`
	rm ${veridxtmp} ${veridxtmp2}
	echo "${tableout} ${word} (${mulver})"
    else
	echo "${tableout} ${word} (${argv[0]})"
    fi
    rm ${vertmp1} ${voltmp1}
    exit 0
fi

for ((ii=0; ii<$verargvs; ii++)); do
    input[$ii]=${HERE}/data/${jver[$ii]}/bible_${jver[$ii]}_${jvol}_${jcha}.dat
    if [[ ! -e ${input[$ii]} ]]; then
	bver[$ii]=`awk -F";" -v tver="${argv[$ii]}" '$1==tver { if (NR==0 || $1=="") exit 1; else print $2}' ${vertab}`
	jver[$ii]=$(printf "%02d" ${bver[$ii]})
	numchap=`ls ${HERE}/data/${jver[0]}/bible_${jver[0]}_${jvolt}_* | wc -l`
	echohelp
	echo "$volume has totally ${numchap} chapters. Input proper chapter."
	rm ${vertmp1} ${voltmp1}
	exit 108
    fi
done

declare -A oneword
declare -A intword
declare -A outlen
declare -A intlen

bwrd0=`echo $word | awk -F":" '{print $2}'`

if [[ -z `echo $bwrd0 | awk -F"-" '{print $2}'` ]]; then
    bwrd1=`echo $bwrd0 | awk -F"-" '{print $1}'`

    for ((ii=0; ii<$verargvs; ii++)); do
	input[$ii]=${HERE}/data/${jver[$ii]}/bible_${jver[$ii]}_${jvol}_${jcha}.dat
	oneword[$ii]=`awk -F"==\t" -v bwrd="$bwrd1" '$1==bwrd {$1=""; print $0}' ${input[$ii]}`
	outlen[$ii]=`echo ${oneword[$ii]} | awk '{print length}'`
    done
    # outlen=${#oneword}
    if [ $outlen -lt 5 ]; then
	inwd=$bwrd1
	while :
	do
	    for ((ii=0; ii<$verargvs; ii++)); do
		input[$ii]=${HERE}/data/${jver[$ii]}/bible_${jver[$ii]}_${jvol}_${jcha}.dat
		intword[$ii]=`awk -F"==\t" -v bwrd="$inwd" '$1==bwrd {$1=""; print $0}' ${input[$ii]}`
		intlen[$ii]=`echo ${intword[$ii]} | awk '{print length}'`
	    done
	    # intlen=${#intword}
	    if [ $intlen -gt 5 ]; then
		for ((ii=0; ii<$verargvs; ii++)); do
		    echo ${intword[$ii]}
		done
		if [[ $verargvs -gt 1 ]]; then
		    veridxtmp=${HERE}/data/veridx.tmp
		    veridxtmp2=${HERE}/data/veridx.tmp2
		    for ((ii=0; ii<$verargvs; ii++)); do
			veridx=$(( $ii ))
			echo "${argv[$veridx]}" >> ${veridxtmp}
		    done
		    cat ${veridxtmp} | tr "\n" "," | sed 's/.$//' > ${veridxtmp2}
		    mulver=`cat ${veridxtmp2}`
		    rm ${veridxtmp} ${veridxtmp2}
		    echo "${tableout} ${bcha}:${inwd} (${mulver})"
		else
		    echo "${tableout} ${bcha}:${inwd} (${argv[0]})"
		fi
		rm ${vertmp1} ${voltmp1}
		break
	    fi
	    (( inwd-- ))
	done
    else 
	for ((ii=0; ii<$verargvs; ii++)); do
	    if [ "${jver[ii]}" = "19" ]; then
		echo -e ${oneword[$ii]} | rev
	    else
		echo ${oneword[$ii]}
	    fi
	done


	if [[ $verargvs -gt 1 ]]; then
	    veridxtmp=${HERE}/data/veridx.tmp
	    veridxtmp2=${HERE}/data/veridx.tmp2
	    for ((ii=0; ii<$verargvs; ii++)); do
		veridx=$(( $ii ))
		echo "${argv[$veridx]}" >> ${veridxtmp}
	    done
	    cat ${veridxtmp} | tr "\n" "," | sed 's/.$//' > ${veridxtmp2}
	    mulver=`cat ${veridxtmp2}`
	    rm ${veridxtmp} ${veridxtmp2}
	    echo "${tableout} ${bcha}:${bwrd1} (${mulver})"
	else
	    echo "${tableout} ${bcha}:${bwrd1} (${argv[0]})"
	fi	
	rm ${vertmp1} ${voltmp1}
    fi
else
    bwrd1=`echo $bwrd0 | awk -F"-" '{print $1}'`
    bwrd2=`echo $bwrd0 | awk -F"-" '{print $2}'`
    nstep=`expr \( $bwrd2 - $bwrd1 + 1 \)`
    
    for ((istep=0; istep<${nstep}; istep++)); do
    	bwrd3=$(( $bwrd1 + $istep ))
	for ((ii=0; ii<$verargvs; ii++)); do
    	    output[$ii]=`awk -F"==\t" -v bwrd="$bwrd3" '$1==bwrd {$1=""; print $0}' ${input[$ii]}`
	    outlen[$ii]=`echo ${output[$ii]} | awk '{print length}'`
	done

	if [ $outlen -lt 1 ]; then
	    bwrd4=$(( $bwrd3 - 1 ))
	    break
	fi

	for ((ii=0; ii<$verargvs; ii++)); do
	    if [ "${jver[ii]}" = "19" ]; then
		echo -n "$bcha:$bwrd3   " && echo -e "${output[$ii]}" | rev
	    else
		echo "$bcha:$bwrd3   ${output[$ii]}"
	    fi
	done

	if [[ $verargvs -gt 1 ]]; then
	    echo ""
	fi
    done
    
    if [[ -z `awk -F"==\t" -v bwrd="$bwrd2" '$1==bwrd {print $2}' ${input}` ]]; then
	bwrd4=$(( $bwrd3 - 1 ))
    else
	bwrd4=$(( $bwrd3 ))
    fi

    if [[ $verargvs -gt 1 ]]; then
	veridxtmp=${HERE}/data/veridx.tmp
	veridxtmp2=${HERE}/data/veridx.tmp2
	for ((ii=0; ii<$verargvs; ii++)); do
	    veridx=$(( $ii ))
	    echo "${argv[$veridx]}" >> ${veridxtmp}
	done
	cat ${veridxtmp} | tr "\n" "," | sed 's/.$//' > ${veridxtmp2}
	mulver=`cat ${veridxtmp2}`
	rm ${veridxtmp} ${veridxtmp2}
	echo "${tableout} ${bcha}:${bwrd1}-${bwrd4} (${mulver})"
    else
	echo "${tableout} ${bcha}:${bwrd1}-${bwrd4} (${argv[0]})"
    fi

    rm ${vertmp1} ${voltmp1}
fi
