#!/bin/bash
#-----------------

for g in *.sh; do
    echo ${g}
    sed -i "s/Users/home/" ${g}  # Linux
    # sed -i 's/python3.9/python3.8/g' ${g} # Linux
    # sed -i 's/python3.8/python3.9/g' ${g} # abigail old
    sed -i 's/python3.9/python/g' ${g} # abigail new
    sed -i 's/#!\/usr\/local\//#!\//g' ${g} # Linux
    # sed -i '' 's/home/Users/g' ${g} # macOS
    # sed -i '' 's/python3.8/python3.9/g' ${g} # macOS
    # sed -i '' 's/#!\//#!\/usr\/local\//g' ${g} # macOS
done

for g in *.py; do
    echo ${g}
    sed -i 's/Users/home/' ${g}  # Linux
    sed -i 's/python3.9/python3.8/g' ${g} # Linux
    sed -i 's/#!\/usr\/local\//#!\//g' ${g} # Linux
    # sed -i '' 's/home/Users/g' ${g} # macOS
    # sed -i '' 's/python3.8/python3.9/g' ${g} # macOS
    # sed -i '' 's/#!\//#!\/usr\/local\//g' ${g} # macOS
done
