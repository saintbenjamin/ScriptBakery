#!/bin/bash
#----------

find . -name "*.[Ch]" -not -path "./lib/freeze*" -not -path "./lib/module-gsl/freeze*" | xargs ctags -eR
find . -name "*.[Ch]" -not -path "./lib/freeze*" -not -path "./lib/module-gsl/freeze*" | xargs ctags -R
find . -name "*.[ch]" -not -path "./lib/freeze*" -not -path "./lib/module-gsl/freeze*" | xargs ctags -eR
find . -name "*.[ch]" -not -path "./lib/freeze*" -not -path "./lib/module-gsl/freeze*" | xargs ctags -R
