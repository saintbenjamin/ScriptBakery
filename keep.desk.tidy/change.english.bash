#!/bin/bash
#----------

cd tmp2_flac
for f in *.flac; do
    ffmpeg -i "$f" -i 1200x1200bf-60.jpg -map 0:a -map 1 -codec copy -metadata:s:v title="Album cover" -metadata:s:v comment="Cover (front)" -disposition:v attached_pic "${f%.flac}.flac"
done
cd -
