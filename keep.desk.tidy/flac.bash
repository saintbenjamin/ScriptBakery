#!/bin/bash
#----------

for g in *; do
    echo "${g}";
    if [ ! -d "${g}_flac" ]; then
	mkdir "${g}_flac";
    fi
    cd "${g}";
    for f in *.m4a; do
	ffmpeg -i "$f" "${f%.m4a}.flac" -y;
	mv "${f%.m4a}.flac" ../"${g}_flac"/.
    done
    cd ..
done
