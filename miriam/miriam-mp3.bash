#!/bin/bash
#-----------------

echorun()
{
    echo "`date`"
    echo "$1"
    $1
}

argv=(`xsel --clipboard --output`)
argcount=${#argv[@]}
if [[ -z ${argv[0]} ]]; then
    echo "Plese input the proper youtube address"
    exit 1    
elif [[ -z `echo ${argv[0]} | awk -F"?v=" '{print $2}'` ]]; then
    if [[ -n `basename ${argv[0]}` ]]; then  
	sfx=`basename ${argv[0]}`
    else
	echo "Plese input the proper youtube address"
	exit 1
    fi
else
    sfx=`echo ${argv[0]} | awk -F"?v=" '{print $2}' | awk -F"&" '{print $1}'`
fi

youtube-dl -F ${sfx} |& tee opt.tmp
optnum=`youtube-dl -F ${sfx} | grep m4a  | grep tiny | awk '{print $1}'`
rm opt.tmp
echorun "youtube-dl -f $optnum $sfx"

for g in *.m4a; do
    ffmpeg -i "${g}" -acodec libmp3lame -aq 2 "${g%.m4a}.mp3" -y
    mv "${g%.m4a}.mp3" /home/benjamin/Dropbox/Suite_SNU/.
    rm "${g}"
done
