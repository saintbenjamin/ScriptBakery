#!/bin/bash
#-----------------

echorun()
{
    echo "`date`"
    echo "$1"
    $1
}

argv=(`xsel --clipboard --output`)
# argv=(`powershell.exe -command 'Get-Clipboard'`)
argcount=${#argv[@]}
if [[ -z ${argv[0]} ]]; then
    echo "Plese input the proper youtube address"
    exit 1    
elif [[ -z `echo ${argv[0]} | awk -F"?v=" '{print $2}'` ]]; then
    if [[ -n `basename ${argv[0]}` ]]; then  
	sfx=`basename ${argv[0]}`
    else
	echo "Plese input the proper youtube address"
	exit 1
    fi
else
    sfx=`echo ${argv[0]} | awk -F"?v=" '{print $2}' | awk -F"&" '{print $1}'`
fi

if (( `youtube-dl -F ${sfx} | grep 1080 | grep mp4 | wc -l` ==1 )); then
    optvid=`youtube-dl -F ${sfx} | grep 1080 | grep mp4 | awk '{print $1}'`
    optaud=`youtube-dl -F ${sfx} | grep m4a  | grep tiny | awk '{print $1}'`
    echorun "youtube-dl -f $optvid $sfx"
    echorun "youtube-dl -f $optaud $sfx"
    for f in *.mp4; do
	for g in *.m4a; do
	    ffmpeg -i "${f}" -i "${g}" -vcodec copy -acodec copy "${f%.mp4}_.mp4" -y
	    mv "${f%.mp4}_.mp4" /home/benjamin/Dropbox/Suite_SNU/"${f}"
	    rm "${g}"
	done
	rm "${f}"
    done
elif (( `youtube-dl -F ${sfx} | grep 720 | grep mp4 | wc -l` ==1 )); then
    optvid=`youtube-dl -F ${sfx} | grep 720 | grep mp4 | awk '{print $1}'`
    optaud=`youtube-dl -F ${sfx} | grep m4a | grep tiny | awk '{print $1}'`
    echorun "youtube-dl -f $optvid $sfx"
    echorun "youtube-dl -f $optaud $sfx"
    for f in *.mp4; do
	for g in *.m4a; do
	    ffmpeg -i "${f}" -i "${g}" -vcodec copy -acodec copy "${f%.mp4}_.mp4" -y
	    mv "${f%.mp4}_.mp4" /home/benjamin/Dropbox/Suite_SNU/"${f}"
	    rm "${g}"
	done
	rm "${f}"
    done
else
    youtube-dl -F ${sfx} |& tee opt.tmp
    optnum=`grep -i best opt.tmp | awk '{print $1}'`
    rm opt.tmp    
    echorun "youtube-dl -f $optnum $sfx"
    
    for g in *.mp4; do
	mv "${g}" /home/benjamin/Dropbox/Suite_SNU/.
    done
fi
